// console.log("Hello World!");

// [SECTION] - Objects
/* SYNTAX: 
let/const objectName = {
	keyA: valuA,
	keyB: valueB 
}
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects using initializers/literal notation");
console.log(cellphone);
console.log(typeof cellphone);

// This is an object

function Laptop(name, manufactureDate){
	//use this.keyName to add property
	this.name = name,
	this.manufactureDate = manufactureDate
}

// This is an instance of an object
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating object using object constructors: ");
console.log(laptop);

let myLaptop = new Laptop("MacBook Air", 2020);
console.log("Result from creating object using object constructors: ");
console.log(myLaptop);

//we cannot create new object without the instance or the "new" keyword
let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result from creating object using object constructors: ");
console.log(oldLaptop); //return undefined value

// Creating empty objects
let computer = {}
console.log(computer);

let array = [laptop, myLaptop];

//this is confusing because of the array method
console.log(array[0]["name"]);
//properway of accessing object property value is by using dot notation
console.log(array[0].name);

// [SECTION] - Initializing, Deleting, Re-assigning Object Properties

let car = {};

// Adding key pairs
car.name = "Honda Civic";
console.log(car)

// We can objects with:

// {} - object literal - great for creating dynamic objects
let pokemon1 = {
	name: "Pikachu",
	type: "Electric"
}

let pokemon10 = {
	name: "Mew",
	type: ["Pyschic", "Normal"]
}

let pokemon25 = {
	name: "Charizard",
	level: 30
}
// constructor function - allows us to create object with a defined structure

function Pokemon(name,type,level){

	//this - keyword used to refer to the object to be created with the contrstructor function

	this.name = name;
	this.type = type;
	this.type = level;
}
//new keyword allows us to create an instance of an object with a constructor function

let pokemonInstance1 = new Pokemon ("Bulbasaur", "Grass", 32);

//Objects can have arrays as properties:

let professor1 = {
	name: "Romenick Garcia",
	age: 25,
	subjects: ["MongoDB", "HTML", "CSS","JS"]
}

//We can access object properties with dot notation
console.log(professor1.subjects);

//what if we want to add an item in the array within the object?
//what if we want to add "nodeJS" as subject for professor1?
professor1.subjects.push("NodeJS");
console.log(professor1.subjects);

//Mini-Activity: display each subject from professor1's subjects array:
professor1.subjects.forEach(function (subjects){
	console.log(subjects);
})

//approach 1 - access the items from the array using its index:
// console.log(professor1.subjects[0]);
// console.log(professor1.subjects[1]);
// console.log(professor1.subjects[2]);
// console.log(professor1.subjects[3]);


//approach 2 - we can also use forEach() to display each item in the array:
//professor1.subjects.forEach(subject => console.log(subjects));
/*professor1.subjects.forEach(function (subjects){
	console.log(subjects);
})*/

let dog1 = {
	name: "Bantay",
	breed: "Golden Retriever"
};
let dog2 = {
	name: "Bolt",
	breed: "Aspin"
};

//Array of objects:

let dogs = [dog1,dog2];

// how can we display the details of the first item in the dogs array
// you can use the index number item to access it from the array:
console.log(dogs[0])
//how can we display the value of the name of the property of the second item?
console.log(dogs[1].name);
console.log(dogs[1]["name"]);

//what if we want to delete the second item from the dogs array?
dogs.pop();
console.log(dogs);

dogs.push({

	name:"Whitney",
	breed: "Shih Tzu"

})
console.log(dogs);

//initialize,delete,update object properties

let supercar1 = {};

console.log(supercar1);

//initialize properties and values with our empty object:
supercar1.brand = "Porsche";

console.log(supercar1);

supercar1.model = "Porsche 911";
supercar1.price = 182000;

console.log(supercar1);

//Delete object properties with the delete keyword
delete supercar1.price;
console.log(supercar1);

//Update the values of an object using the dot notation
supercar1.model = "718 Boxster";

console.log(supercar1);

pokemonInstance1.type = "Grass, Normal";
console.log(pokemonInstance1)

//Object Methods
//Functions in an object
//This is useful for creating functions that are associated to a specific object

let person1 = {
	name: "Joji",
	talk: function(){
		console.log("Hello!");
	}
};

person1.talk();

let person2 = {
	name: "Joe",
	talk: function(){
		console.log("Hello, World!")
	}
}

person2.talk();

person2.walk = function (){
	console.log("Joe has walked 1000 miles just to be the man that walked 1000 miles to be at your door.");
}

person2.walk();

//this keyword in an object method refers to the current object where the method is

person1.introduction = function(){
	console.log("Hi! I am" + " " +this.name +"!");
}

person1.introduction();

person2.introduction = function(){
	console.log(this);
}

person2.introduction();

//mini-activity
//create a new object as student1
// the object should have the following properties:
//name,age,address
//add 3 methods for student1
//introduceName = which will display the student1's name as :
//"Hello! my name is <nameofstudent1>"
//introduceAge = which will display the age of student1 as:
//"Hello I am <ageofstudent1> years old."
//introduceAddress = which will dispalye the address of student1 as:
//"Hello! I am <nameofstudent1>. I live in <addressofstudent1>"

/*let student1= {
	name:"Ken",
	age:12,
	address: "New York Street, Tondo, Manila"
}

student1.introduceName = function(){
	console.log("Hello! my name is " + this.name);
}

student1.introduceAge = function(){
	console.log("Hello! I am " + this.age +" years old.")
}

student1.introduceAddress = function(){
	console.log("Hello! I am "+ this.name +". I live in " + this.address)
}

student1.introduceName();
student1.introduceAge();
student1.introduceAddress();*/

function Student(name,age,address){
	this.name = name;
	this.age = age;
	this.address = address

	//We can also add methods to our constructor function
	this.introduceName = function(){
		console.log("Hello! my name is " + this.name);
	};
	this.introduceAge = function(){
		console.log("Hello! I am " + this.age +" years old.")
	}
	this.introduceAddress = function(){
		console.log("Hello! I am "+ this.name +". I live in " + this.address)
	}

	//We can also add methods that take other object as an agrument
	this.greet = function(person){
		//person's properties are accessible in the method
		console.log("Good Day, " + person.name + "!");
	}
}
let newStudent1 = new Student("Ken", 22, "Taytay")
console.log(newStudent1);

let newStudent2 = new Student("Jeremiah", 26 ,"Lucena, Quezon")

newStudent2.greet(newStudent1);

//Mini activity
//create a new constructor function called dog which is able to craete objects with the following properties:
//name,breed
//the constructor function should have 2 methods:
//call() - is a method which will allow us to display the following message:
//"Bark Bark Bark!"
//greet() - is a method which will allow us to greet another person. this method should be able to receive an object which has a name property. Upon invoking the greet() method it should display:
//"Bark, bark, <nameOfPerson>"

//create an instance/object from the dog constructor
//invoke its call() method
//invocke its greet method to greet newStudent2

//take a screenshot of your output and send it to the hangouts.
function Dog(name,breed){
	this.name = name;
	this.breed = breed;

	this.call = function(){
		console.log("Bark Bark Bark!")
	}

	this.greet = function(person){
		//console.log(person.hasOwnProperty("name")) //will return t/f
		console.log("Bark, bark, " + person.name)
	}
}

let newDog1 = new Dog("Barbie", "Pug");
newDog1.call();
newDog1.greet(newStudent1);
// newDog1.greet(supercar1); //undefined

function sample1(sampleParameter){
	newDog1.name = "Gary";
	console.log(sampleParameter.name);
}

sample1(newDog1);
console.log(newDog1);
// sample1(25000); //undefined


